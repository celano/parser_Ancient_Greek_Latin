distinct-values
(doc("../original_texts/all_texts_of_merge.xml")//word/@relation
!
replace(., "_CO", "")
!
replace(., "_AP", "")
!
 replace(., "_ExD.*", "")
 !
  replace(., "_PA", "")
 !
 replace(.,"ExD_Exd0_PRED", "ExD")
 !
 (if (string-length(.) =2) then replace(.,"CO", "UNDEFINED") else .)
)