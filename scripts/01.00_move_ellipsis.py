from lxml import etree

# all elliptical nodes have @artificial

def check_ellipsis(sent_element):
    truth_value = False
    words = s.findall(".//word")
    for w in words:
      if "artificial" in w.attrib.keys():
        truth_value = True
    return truth_value

path = "../normalized_texts/all_sentences_greek.xml"

tree = etree.parse(path)

# assumption : all elliptical nodes are at the end of a sentence
el_sent = tree.findall(".//sentence")
modified_sentences = []
for s in el_sent:
  if check_ellipsis(s): # if there is ellipsis the sentence is returned
    words = s.findall(".//word")
    ellipsis = [w for w in words if "artificial" in w.attrib.keys()]
    number_nodes = len(ellipsis)
    for numb, el in enumerate(ellipsis):
        ellipsis_index = words.index(el)
        preceding = [w for w in words if words.index(w) == ellipsis_index - 1]
        ellipsis_id = el.attrib["id"]
        preceding_id = preceding[0].attrib["id"]
        el.attrib["id"] = preceding_id
        preceding[0].attrib["id"] = ellipsis_id
        for w in words:
          if w.attrib["head"] == preceding_id:
             w.attrib["head"] = ellipsis_id
          elif  w.attrib["head"] == ellipsis_id:
             w.attrib["head"] = preceding_id
        a , b = words.index(preceding[0]), words.index(el)
        words[b], words[a] = words[a], words[b]
    new_s = etree.Element("sentence")
    for k,v in s.items():
       new_s.attrib[k] = v
    for w in words:
       new_s.append(w)
    modified_sentences.append(new_s)
  else:
    modified_sentences.append(s)

treebank = etree.Element("treebank")
for s in modified_sentences:
    treebank.append(s)
t = etree.tostring(treebank, encoding="UTF-8", pretty_print=True)
tt = t.decode("utf-8")
with open('../normalized_texts/sentences_ellipsis_ordered_greek.xml', "w") as f:
  print(tt, file=f)
