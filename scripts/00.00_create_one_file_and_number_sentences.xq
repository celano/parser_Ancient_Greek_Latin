(: Basex 10.7 :)

copy $t := 

<treebank>{collection("../original_texts/merge")//sentence}</treebank>

modify
(
  for $s at $count in $t//sentence
  return
  (insert node attribute absolute_id {$count} into $s,
   insert node attribute text {$s//word/@form} into $s) 
)
return
$t