(: Basex 10.7 :)

declare option output:indent 'yes';

declare variable $p := 
doc("../normalized_texts/sentences_ellipsis_ordered_greek.xml");

(: 
error in the treebank: some apheresis is treated as a separate token 
and encoded as &#x0313; or rarely as &#x1fbd;
:)

declare function local:normalize($collection)
{
      for $s in $p//sentence
      return
      element s {$s/@*,
      
       for $t in $s/word
       return
       if (not($t/@artificial)) then
       <t ell="no">
       {
       $t/@* 
       }
       </t>
       else	
       <t ell="yes">
       {
       $t/@*
       }
       </t>
       }
};

(:
declare function local:punctuation($sentences)
{
      for $s in $sentences
      return
      element s {$s/@*,
      
       for $t in $s/t
       return
       if (matches($t/@form, "\p{P}") and string-length($t/@form) = 1) then
       <t punct="yes">{$t/@*}</t>
       else
       <t punct="no">{$t/@*}</t>
      }
      
};
:)

declare function local:group-ellipsis($sentences)
{
     for $s in $sentences
     return
     element s {$s/@*,
     
      for $t in $s/t
      group by $g1 :=$t/@ell
      return
      <g1 ell="{$g1}" multi="{string-join($t/@form, " ")}">
      {$t}
      </g1>
     }
     
};

(: this is to replace cases of [3] [2] [1] into [0] [1] [2] :)
declare function local:rename-form-ellipsis($sentences)
{
 for $s in $sentences
 let $g1n := $s/g1[@ell="no"]
 let $g1y := for $t at $count in $s/g1[@ell="yes"]//t
             return 
             element t {$t/@* except ($t/@form, $t/@lemma),
                       attribute form  {"["|| $count - 1 ||"]"},
                       attribute lemma {"["|| $count - 1 ||"]"},
                       attribute original_form {$t/@form}} 
 return
 if ($s/g1[@ell="yes"])
 then
 element s {$s/@*,
 ($s/g1[@ell="no"],
 <g1 ell="yes" multi="{string-join(for $t in $g1y return $t/@form, " ")}">
 {$g1y}
 </g1>
 )
 }
 else 
 $s
};

declare function local:merge-ellipsis($p) 
{
     for $s in local:normalize($p)=> (:local:punctuation() =>:) 
     local:group-ellipsis()
     => local:rename-form-ellipsis()
     return
      element s {$s/@*,
      
      for $t in $s//t
      order by xs:integer($t/@id)
      return $t

      }
};


(

for $s at $n_sent in local:merge-ellipsis($p)
return

("# absolute_id = " || $s/@absolute_id,
  "# doc_id = " || $s/@doc_id,
 "# sent_id = " || $s/@id,
  "# text = " || $s/@text,
for $t in $s//t
return

string-join(($t/@id, $t/@form, $t/@lemma, substring($t/@postag, 1,1), 
$t/@postag,

string-join
(

let $r := analyze-string($t/@postag, ".")//*[position() > 1]
return 

 (if ($r[7] != "-")  then "Case=" || $r[7],
  if ($r[8] != "-")  then "Degree=" || $r[8],
  if ($r[6] != "-")  then "Gender=" || $r[6],
  if ($r[4] != "-")  then "Mood=" || $r[4],
  if ($r[2] != "-")  then "Number=" || $r[2],
  if ($r[1] != "-")  then "Person=" || $r[1],
  if ($r[3] != "-")  then "Tense=" || $r[3],
  if ($r[5] != "-")  then "Voice=" || $r[5]
), "|")  
,
$t/@head, 
$t/@relation, "_", "_"), 
"	"), 
"" 
),
(: final newline :)

""

)

                                