import random

random.seed(342776)

path = "../normalized_texts/sentences_greek.conllu"

with open(path) as f:
  data = f.read()

sentences = data.split("\n\n")
random.shuffle(sentences)

train = sentences[0: round(len(sentences) * 0.60)]
test__ = sentences[round(len(sentences) * 0.60) :]
dev = test__[0:round(len(test__)/2)]
test = test__[round(len(test__) /2) : ]

with open("../normalized_texts/train.conllu", "w") as f:
  print("\n\n".join(train), end="\n\n", file=f)

with open("../normalized_texts/dev.conllu", "w") as f:
  print("\n\n".join(dev), end="\n\n", file=f)

with open("../normalized_texts/test.conllu", "w") as f:
  print("\n\n".join(test), end="\n\n", file=f)
