import numpy as np
import tensorflow as tf
import tensorflow_text as text
from tensorflow import keras
from tensorflow.keras import layers
from transformers import AutoTokenizer, TFAutoModel

ud_train_p = "../UD_Ancient_Greek-Perseus-master/grc_perseus-ud-train.conllu"
ud_dev_p = "../UD_Ancient_Greek-Perseus-master/grc_perseus-ud-train.conllu"

#my_train_p = "../normalized_texts/train.conllu"
#my_dev_p = "../normalized_texts/dev.conllu"

def prep_text(conllu_file):
  with open (conllu_file) as f:
    data = f.read()
    # delete last \n\n
    data = data[0:-2]
  sentences = data.split("\n\n")
  sents = []
  for s in sentences:
    words = s.split("\n")
    sent = []
    fields = filter(lambda x: not x.startswith("#"), words)
    for w in fields:
      fields_ = filter(lambda x: x.startswith("#"), words)
      word = dict()
      for q in fields_:
        sd = q.split(" = ")
        word[sd[0].replace("# ", "")] = sd[1]
      lines = w.split("\t")
      for n, f in enumerate(lines):
         #if f.startswith("#"):
         #   pass
         if n == 0:
              word["id"] = f
         elif n == 1:
              word["form"] = f
         elif n == 2:
              word["lemma"] = f
         elif n == 3:
              word["upos"] = f
         elif n == 4:
              word["xpos"] = f
         elif n == 5:
              word["feats"] = f
         elif n == 6:
              word["head"] = f
         elif n == 7:
              word["deprel"] = f
         elif n == 8:
              word["deps"] = f
         elif n == 9:
              word["misc"] = f
      sent.append(word)
    sents.append(sent)
  return sents 

ud_train = prep_text(ud_train_p)
ud_dev = prep_text(ud_dev_p)
#my_train = prep_text(my_train_p)
#my_dev = prep_text(my_dev_p)

# MODEL

train_sents = [x[0]["text"] for x in ud_train]
tokenizer = AutoTokenizer.from_pretrained("pranaydeeps/Ancient-Greek-BERT")
tokenized = tokenizer(train_sents, return_tensors="tf", padding="longest")
tokenized = tokenizer(train_sents, return_tensors="tf", padding="max_length", max_length=300)
max_length = tokenized["input_ids"].shape[1] 



model_pre = TFAutoModel.from_pretrained("pranaydeeps/Ancient-Greek-BERT", 
                                        from_pt=True)
