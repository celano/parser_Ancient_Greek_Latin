# Analytes: a Parser for Ancient Greek and Latin

The repository contains the data used to train the parser Analytes.

The dataset used for training consists of the texts in `/merge` (also
used to train the model *Elleipsis*). All files have been merged in
`all_texts_of_merge.xml`, where a few minor mistakes have been
corrected manually (therefore, the file differs from the `/merge` files).
More consistent errors/annotation inconsistencies have required a script.

The original texts have been changed in order to make the tokenization scheme
more consistent in itself and with that of *Opera Graeca Adnotata* and
correct those errors that would otherwise impact machine learning
significantly (for example, number of categories to predict for classification
tasks). More precisely:

* The apostrophe encoding has been normalized to MODIFIER LETTER APOSTROPHE
(U+02BC), and conjunctions such as "οὔτε" have always been split. Crases,
which are more difficult to identify and automatically annotate, have not
been normalized (sometimes they are split, sometimes they are not), 
but their frequency in the database is estimated to be 
just about 0.2%.

* Word forms and lemmas have been NFC normalized. Some of them
contained a dash to signal that the token was not an original one. Such
dashes have been deleted for consistency with the tokenization scheme of
*Opera Graeca Adnotata*. 571 (non-elliptical) words were missing a lemma 
and they have been substituted with their corresponding word forms.

* `@postag` has been cleaned and normalized: a few mistakes were found with
respect to the admissible values for each category, while other values, 
which were used only for a very few examples, have been substituted (for 
example, "p" used in the category `degree` to mean `positive` was sporadically
used, with most entries simply leaving that value as `-`; similarly, `c` in
`gender` was used only for a very few examples).

* `@relation` was normalized and simplified: the very rare `MWE` label was 
substituted, while the `_CO` and `_AP` suffixes have been deleted, in that they
encode redundant information, which simply highly increases 
the number of possible syntactic labels. Similarly, the few `*_ExD_*` 
used to annotate an elliptical node were deleted. If a value was missing or
was `XSEG`, this has been substituted with `UNDEFINED` 
(in total, about 800 tokens).

* `@head` has a few missing values (about 12): 
they have been substitute with `0`.

Finally,
the elliptical nodes, which were originally added at the end of a sentence 
after the final punctuation mark, have been moved before it. Moreover,
their forms and lemmas have been always been transformed into the schema
`[x]`, with `x` been an integer, as was made for the training dataset 
of *Elleipsis*.

The dataset has been divided into train (80%) dev (20%) and test (20%) sets.
